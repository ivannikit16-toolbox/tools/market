﻿#if HIVE_APPODEAL
using System;
using UnityEngine;
using System.Collections;
using Hive.Ads;

namespace Toolbox.Market
{
    public class AppodealRewardedVideoPurchase : Purchase
    {
        private bool _requestInProgress = false;

        public AppodealRewardedVideoPurchase(string key, IProduct product) : base(key, product)
        {
        }

        public override bool IsLoaded() { return Appodeal.IsLoaded(AppodealAdType.RewardedVideo); }

        public override string GetPrice() { return string.Empty; }

        public override void Load(Action cb)
        {
            if (IsLoaded())
                cb.TryInvoke(LogMode.Error);
            else
                Game.Instance.StartCoroutine(LoadingCoroutine(cb));
        }

        private IEnumerator LoadingCoroutine(Action cb)
        {
            if (!IsLoaded())
            {
                Appodeal.Load(AppodealAdType.RewardedVideo);

                while (!IsLoaded())
                    yield return null;
            }

            cb.TryInvoke(LogMode.Error);
        }

        public override void Get(Action<bool> cb)
        {
            if (!IsLoaded() || _requestInProgress)
            {
#if DEBUG
                Debug.LogFormat("AppodealPurchase->Get breake IsLoaded: {0}", IsLoaded());
#endif
                cb.TryInvoke(false, LogMode.Error);
                return;
            }

#if DEBUG
            Debug.Log("RewardedVideo calling");
#endif
            _requestInProgress = true;
            Appodeal.ShowAd(AppodealAdType.RewardedVideo, (adType, args) =>
           {
#if DEBUG
                Debug.LogFormat("RewardedVideo result: {0}", args.ResultCode);
#endif
                bool succeeded = args.Watched;
               if (succeeded)
                   Product.Consume();

               _requestInProgress = false;
               cb.TryInvoke(succeeded, LogMode.Error);
           });
        }
    }
}
#endif