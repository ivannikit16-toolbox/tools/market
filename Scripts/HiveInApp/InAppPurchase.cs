﻿#if HIVE_INAPP_PURCHASES
using System;
using UnityEngine;
using System.Collections.Generic;
using Hive.InAppPurchases;
using HiveInAppPurchases = Hive.InAppPurchases.InAppPurchases;
using HiveInAppProduct = Hive.InAppPurchases.InAppProduct;

namespace Toolbox.Market
{
    public class InAppPurchase : Purchase
    {
        private HiveInAppProduct _hiveProduct;
        public HiveInAppProduct GetHiveInAppProduct() { return _hiveProduct; }

        private bool _requestInProgress = false;

        public InAppPurchase(string key, IProduct product, HiveInAppProduct hiveProduct) : base(key, product)
        {
            if (hiveProduct == null)
                throw new ArgumentNullException("hiveProduct");

            _hiveProduct = hiveProduct;
        }

        private void AssertNotNullProduct()
        {
            if (_hiveProduct == null)
                throw new NullReferenceException("_hiveProduct");
        }

        public override bool IsLoaded()
        {
            AssertNotNullProduct();
            return _hiveProduct.State == InAppProductState.Updated;
        }

        public override string GetPrice()
        {
            AssertNotNullProduct();
            string price = string.Empty;
            if (IsLoaded())
            {
                price = _hiveProduct.FormattedPrice;
                price = Product.ModifyPrice(price);
            }

            return price;
        }

        public override void Load(Action cb)
        {
            AssertNotNullProduct();

            if (IsLoaded() || _requestInProgress)
            {
                cb.TryInvoke(LogMode.Error);
                return;
            }

            _requestInProgress = true;
            InAppEventHandler handler = (sender, args) =>
            {
                Debug.LogFormat("InApp id:{0} price:{1}", _hiveProduct.Identifier, _hiveProduct.Price);
#if DEBUG
                Debug.Log(args.ToString());
#endif
                _requestInProgress = false;
                cb.TryInvoke(LogMode.Error);
            };
            HiveInAppPurchases.RequestProductsData(new[] { _hiveProduct }, handler);
        }

        public override void Get(Action<bool> cb)
        {
            AssertNotNullProduct();

            if (!IsLoaded() || _requestInProgress)
            {
                cb.TryInvoke(false, LogMode.Error);
                return;
            }

            if (_hiveProduct.State != InAppProductState.Updated)
            {
                cb.TryInvoke(false, LogMode.Error);
                Load(null);
                return;
            }

            _requestInProgress = true;
            HiveInAppPurchases.PurchaseProduct(_hiveProduct, (sender, args) =>
           {
#if DEBUG
                Debug.Log(args.ToString());
#endif
                if (args.Succeeded)
               {
                   Product.Consume();
                   OnGetSucceeded.TryInvoke();
               }

               _requestInProgress = false;
               cb.TryInvoke(args.Succeeded, LogMode.Error);
           });
        }

        public static event Action OnGetSucceeded;

        public override bool Restore(Dictionary<string, List<string>> data)
        {
            AssertNotNullProduct();

            bool result = false;
            List<string> ids;
            if (data.TryGetValue(InAppRestoreService.TAG, out ids))
            {
                if (ids.Contains(_hiveProduct.Identifier))
                {
                    if (!Product.IsConsumed())
                        Product.Consume(true);

                    result = true;
                }
            }

            return result;
        }
    }
}
#endif