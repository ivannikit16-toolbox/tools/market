﻿#if HIVE_INAPP_PURCHASES
using System;
using HiveInAppPurchases = Hive.InAppPurchases.InAppPurchases;

namespace Toolbox.Market
{
    public class InAppRestoreService : IRestoreService
    {
        public void GetRestorePurchaseData(Action<string, string[]> cb)
        {
            HiveInAppPurchases.RestorePurchases((sender, args) =>
           {
               string tag = GetRestoreServiceTag();

               if (args.Succeeded)
               {
                   UnityEngine.Debug.Log("RestorePurchases res Succeeded");

                   string[] identifiers = new string[args.Products.Length];
                   for (int i = 0; i < args.Products.Length; i++)
                   {
                       identifiers[i] = args.Products[i].Identifier;
                       UnityEngine.Debug.LogFormat("Restored: {0}", identifiers[i]);
                   }

                   cb.TryInvoke(tag, identifiers, LogMode.Error);
               }
               else
                   cb.TryInvoke(tag, null, LogMode.Error);
           });
        }

        public const string TAG = "IN_APP";
        public string GetRestoreServiceTag() { return TAG; }
    }
}
#endif