﻿using System;
using Toolbox.Events;
using Toolbox.Inspector;
using Toolbox.View;
using UnityEngine;
using UnityEngine.Events;

namespace Toolbox.Market.View.Handlers
{
    [Serializable]
    public abstract class Handler : IHandler
    {
        [SerializeField]
        [HideInInspector]
        private bool _enabled = true;
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        [SerializeField]
        [HideInInspector]
        private bool _expand = true;
        public bool Expand
        {
            get { return _expand; }
            set { _expand = value; }
        }

        public abstract void Attach(ProductItem item);
        public abstract void Remove(ProductItem item);
        public virtual void Reset() { }
    }

    [Serializable]
    public class LoadingEventPart : Handler
    {
        public UnityEvent StartLoad;
        public UnityEvent EndLoad;

        public override void Attach(ProductItem item)
        {
            item.OnStartLoad += OnStartLoad;
            item.OnEndLoad += OnEndLoad;
        }

        public override void Remove(ProductItem item)
        {
            item.OnStartLoad -= OnStartLoad;
            item.OnEndLoad -= OnEndLoad;
        }

        private void OnStartLoad() { StartLoad.Invoke(); }
        private void OnEndLoad() { EndLoad.Invoke(); }
    }

    [Serializable]
    public class LoadingIndicatorPart : Handler
    {
        [TypeRistriction(typeof(IVisible))]
        [SerializeField]
        private UnityEngine.Object _view;

        [SerializeField]
        public bool _playIfNotLoaded = false;

        public override void Attach(ProductItem item)
        {
            if (_view == null)
                throw new ArgumentNullException("_view");

            item.OnStartLoad += OnStartLoad;
            item.OnEndLoad += OnEndLoad;
            item.OnLoadedStateChanged += OnLoadedStateChanged;
        }

        public override void Remove(ProductItem item)
        {
            item.OnStartLoad -= OnStartLoad;
            item.OnEndLoad -= OnEndLoad;
            item.OnLoadedStateChanged -= OnLoadedStateChanged;
        }

        private void OnStartLoad()
        {
            (_view as IVisible).SetShowed(true);
        }

        private void OnEndLoad()
        {
            if (!_playIfNotLoaded)
                (_view as IVisible).SetShowed(false);
        }

        private void OnLoadedStateChanged(bool loaded)
        {
            (_view as IVisible).SetShowed(_playIfNotLoaded && !loaded);
        }
    }

    [Serializable]
    public class ConsumedEventPart : Handler
    {
        public UnityEvent OnSetConsumed;
        public UnityEvent OnSetNotConsumed;

        public override void Attach(ProductItem item)
        {
            OnChanged(false);
            item.OnConsumedStateChanged += OnChanged;
        }

        public override void Remove(ProductItem item)
        {
            item.OnConsumedStateChanged -= OnChanged;
        }

        private void OnChanged(bool value)
        {
            if (value)
                OnSetConsumed.Invoke();
            else
                OnSetNotConsumed.Invoke();
        }
    }

    [Serializable]
    public class TextPricePart : Handler
    {
        public string Empty = "none";
        public string Format = string.Empty;

        public StringUnityEvent OnPriceChanged;

        public override void Attach(ProductItem item)
        {
            OnPriceChanged.Invoke(string.Empty);
            item.OnPriceChanged += PriceChanged;
        }

        public override void Remove(ProductItem item)
        {
            item.OnPriceChanged -= PriceChanged;
        }

        private void PriceChanged(string price)
        {
            if (string.IsNullOrEmpty(price))
            {
                price = Empty;
            }
            else if (!string.IsNullOrEmpty(Format))
            {
                price = string.Format(Format, price);
            }

            OnPriceChanged.Invoke(price);
        }
    }

    [Serializable]
    public class SucceededEventPart : Handler
    {
        public UnityEvent OnSucceeded;
        public UnityEvent OnCancel;

        public override void Attach(ProductItem item)
        {
            item.OnSucceeded += Succeeded;
            item.OnCancel += Cancel;
        }

        public override void Remove(ProductItem item)
        {
            item.OnSucceeded -= Succeeded;
            item.OnCancel -= Cancel;
        }

        private void Succeeded() { OnSucceeded.Invoke(); }
        private void Cancel() { OnCancel.Invoke(); }
    }
}
