﻿using UnityEngine;
using UnityEditor;
using Toolbox.Editor;

namespace Toolbox.Market.View.Handlers
{
    [CustomPropertyDrawer(typeof(Handler), true)]
    public class HandlerPropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty enabled = property.FindPropertyRelative("_enabled");
            if (!enabled.boolValue)
                return 0f;

            SerializedProperty expand = property.FindPropertyRelative("_expand");
            if (expand.boolValue)
                return EditorGUIUtility.singleLineHeight + BACK_BORDER + GetModuleFieldsHeight(property, label);
            else
                return EditorGUIUtility.singleLineHeight + BACK_BORDER;
        }

        private const float BACK_BORDER = 1f;
        private const float PROPERTY_BORDER = 10f;
        private const float PROPERTY_SPACE = 2f;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty enabled = property.FindPropertyRelative("_enabled");
            if (!enabled.boolValue)
                return;

            label = EditorGUI.BeginProperty(position, label, property);

            //border
            GUI.Box(position, GUIContent.none, ParticleSystemStylesWrapper.Get().effectBgStyle);
            Rect field = new Rect(position.x + BACK_BORDER, position.y + BACK_BORDER, position.width - BACK_BORDER * 2f, position.height - BACK_BORDER * 2f);
            GUI.Box(field, GUIContent.none, ParticleSystemStylesWrapper.Get().moduleBgStyle);

            //title
            Rect title = field;
            title.height = EditorGUIUtility.singleLineHeight;
            SerializedProperty expand = property.FindPropertyRelative("_expand");
            if (GUI.Button(title, property.displayName, ParticleSystemStylesWrapper.Get().moduleHeaderStyle))
                expand.boolValue = !expand.boolValue;

            if (expand.boolValue)
            {
                Rect valuesField = field;
                valuesField.yMin += title.height;
                valuesField.x += PROPERTY_BORDER;
                valuesField.width -= PROPERTY_BORDER * 2f;
                valuesField.height += PROPERTY_BORDER;
                DrawModuleFields(valuesField, property, label);
            }

            EditorGUI.EndProperty();
        }

        private void DrawModuleFields(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect rect = position;
            rect.y += PROPERTY_BORDER;
            if (property.hasVisibleChildren)
            {
                int depth = property.depth;
                bool rs = property.NextVisible(true);
                if (rs)
                    rect.y += PROPERTY_SPACE;

                while (rs && property.depth > depth)
                {
                    rect.height = EditorGUI.GetPropertyHeight(property, label);
                    rs = EditorGUI.PropertyField(rect, property);
                    rect.y += rect.height + PROPERTY_SPACE;
                    rs = property.NextVisible(rs);
                }

                rect.y -= PROPERTY_SPACE;
            }
        }

        private float GetModuleFieldsHeight(SerializedProperty property, GUIContent label)
        {
            float height = 0f;
            if (property.hasVisibleChildren)
            {
                int depth = property.depth;
                height += PROPERTY_BORDER;
                bool rs = property.NextVisible(true);

                while (rs && property.depth > depth)
                {
                    height += EditorGUI.GetPropertyHeight(property, label) + PROPERTY_SPACE;
                    rs = property.NextVisible(false);
                }

                height -= PROPERTY_SPACE;
            }

            height += PROPERTY_BORDER;
            return height;
        }

    }
}
