﻿using Toolbox.Inspector;
using System;
using UnityEngine;
using Toolbox.View;

namespace Toolbox.Market.View.Handlers
{
    [Serializable]
    public class WaitingPurchaseHandler : Handler
    {
        //[TypeRistriction( typeof( IVisible ) )]
        [SerializeField]
        private UnityEngine.Object _view;

        public override void Attach(ProductItem item)
        {
            item.OnStartGet += ShowWaiting;
            item.OnEndGet += HideWaiting;
        }

        public override void Remove(ProductItem item)
        {
            item.OnStartGet -= ShowWaiting;
            item.OnEndGet -= HideWaiting;
        }

        private void ShowWaiting()
        {
            (_view as IVisible).SetShowed(true);
            //_view.Get<IVisible>().SetShowed( true );
        }

        private void HideWaiting()
        {
            (_view as IVisible).SetShowed(false);
            //_view.Get<IVisible>().SetShowed( false );
        }
    }
}
