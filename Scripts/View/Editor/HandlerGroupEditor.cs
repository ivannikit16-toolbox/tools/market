﻿using UnityEngine;
using UnityEditor;
using Toolbox.Editor;

namespace Toolbox.Market.View
{
    [CustomEditor(typeof(HandlerGroup), true)]
    [CanEditMultipleObjects]
    public class HandlerGroupEditor : UnityEditor.Editor
    {
        private SerializedProperty _product;
        private HandlerGroup _target;
        private IHandler[] _handlers;
        private string[] _names;

        private void OnEnable()
        {
            _target = target as HandlerGroup;

            _handlers = new IHandler[_target.Handlers.Count];
            _names = new string[_handlers.Length];
            _mask = 0;
            for (int i = 0; i < _handlers.Length; i++)
            {
                _handlers[i] = _target.Handlers[i];
                _names[i] = _handlers[i].GetType().Name;
                if (_handlers[i].Enabled)
                    _mask |= (1 << i);
            }
            _product = serializedObject.FindProperty("_productItem");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            //EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_product);
            serializedObject.ApplyModifiedProperties();
            //if( EditorGUI.EndChangeCheck() )
            //    _dirty = true;

            EditorGUILayout.Space();
            DrawMask();

            DrawFields();

            if (_dirty)
            {
                _dirty = false;
                serializedObject.ApplyModifiedProperties();
            }
        }

        private int _mask;
        private void DrawMask()
        {
            _mask = EditorGUILayout.MaskField("Parts", _mask, _names, ParticleSystemStylesWrapper.Get().popup);
            int i = 0;
            int value = _mask;
            while (i < _handlers.Length)
            {
                bool flag = value == 0 ? false : (value & 1) == 1;
                if (_handlers[i].Enabled != flag)
                {
                    _handlers[i].Enabled = flag;
                    _handlers[i].Expand = flag;
                    _dirty = true;
                }
                value >>= 1;
                ++i;
            }
        }

        bool _dirty = false;
        private int DrawFields()
        {
            int count = 0;

            serializedObject.UpdateIfRequiredOrScript();
            SerializedProperty property = serializedObject.GetIterator();

            bool rs = property.NextVisible(true);
            rs = property.NextVisible(true);
            while (rs)
            {
                EditorGUI.indentLevel = property.depth;
                EditorGUI.BeginChangeCheck();
                rs = EditorGUILayout.PropertyField(property);
                if (EditorGUI.EndChangeCheck())
                    _dirty = true;

                rs = property.NextVisible(rs);
                count++;
            }

            return count;
        }

        private int DrawModules(SerializedObject so)
        {
            int count = 0;

            so.UpdateIfRequiredOrScript();

            SerializedProperty property = so.GetIterator();

            bool rs = property.NextVisible(true);
            while (rs)
            {
                if (property.propertyType != SerializedPropertyType.Generic)
                {
                    rs = property.NextVisible(false);
                    continue;
                }

                //SerializedProperty enabledField = property.FindPropertyRelative( "_enabled" );
                //if( enabledField == null || !enabledField.boolValue )
                //{
                //    rs = property.NextVisible( false );
                //    continue;
                //}

                EditorGUI.indentLevel = property.depth;
                EditorGUI.BeginChangeCheck();

                rs = DrawModuleFields(property, rs);

                if (EditorGUI.EndChangeCheck())
                    _dirty = true;
            }

            return count;
        }

        private bool DrawModuleFields(SerializedProperty property, bool rs)
        {
            EditorGUILayout.BeginVertical(ParticleSystemStylesWrapper.Get().effectBgStyle);
            EditorGUILayout.BeginVertical(ParticleSystemStylesWrapper.Get().moduleBgStyle);

            SerializedProperty expand = property.FindPropertyRelative("_expand");
            bool expandValue = expand.boolValue;

            EditorGUILayout.BeginHorizontal(ParticleSystemStylesWrapper.Get().moduleHeaderStyle);
            if (GUILayout.Button(property.displayName, ParticleSystemStylesWrapper.Get().label))
            {
                expandValue = !expandValue;
                expand.boolValue = expandValue;
            }

            EditorGUILayout.EndHorizontal();
            //GUILayout.Button( property.name, ParticleSystemStylesWrapper.Get().moduleHeaderStyle );

            if (property.hasVisibleChildren)
            {
                int depth = property.depth;
                rs = property.NextVisible(true);
                while (rs && property.depth > depth)
                {
                    rs = EditorGUILayout.PropertyField(property);
                    rs = property.NextVisible(rs);
                }
            }
            else
                rs = property.NextVisible(rs);

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
            return rs;
        }
    }
}
