﻿using System.Collections.Generic;
using UnityEngine;

namespace Toolbox.Market.View
{
    [RequireComponent(typeof(ProductItem))]
    public abstract class HandlerGroup : MonoBehaviour
    {
        [SerializeField]
        [HideInInspector]
        private ProductItem _productItem;
        private ProductItem ProductItem
        {
            get
            {
                if (_productItem == null)
                    _productItem = GetComponent<ProductItem>();
                return _productItem;
            }
        }

        public abstract IList<IHandler> Handlers { get; }

        private void Reset()
        {
            _productItem = ProductItem;
            foreach (IHandler h in Handlers)
                h.Reset();
        }

        private void Start()
        {
            ProductItem item = ProductItem;
            foreach (IHandler h in Handlers)
                if (h.Enabled)
                    h.Attach(item);
        }

        private void OnDestroy()
        {
            ProductItem item = ProductItem;
            foreach (IHandler h in Handlers)
                h.Remove(item);
        }
    }

    public interface IHandler
    {
        bool Enabled { get; set; }
        bool Expand { get; set; }
        void Attach(ProductItem item);
        void Remove(ProductItem item);
        void Reset();
    }
}
