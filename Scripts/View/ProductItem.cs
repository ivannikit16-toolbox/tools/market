﻿using System.Collections;
using UnityEngine;

namespace Toolbox.Market.View
{
    public class ProductItem : MonoBehaviour
    {
        private enum State { Idle, Loading, Getting, LoadingAndGetting }
        private State _state = State.Idle;

        public string Key;
        public bool LoadOnStart = true;
        public bool CallLoadInGet = true;

        private IPurchase _purchase;
        public IPurchase Purchase
        {
            get
            {
                if (_purchase == null)
                    _purchase = PurchaseCollection.Instance.Find(Key);
                return _purchase;
            }
        }

        public bool IsConsumed { get { return _purchase != null && _purchase.IsConsumed(); } }
        public bool IsLoaded { get { return _purchase != null && _purchase.IsLoaded(); } }
        private string GetPrice() { return _purchase != null ? _purchase.GetPrice() : string.Empty; }

        #region Events

        public delegate void OnStateHandler(bool value);
        public delegate void OnPriceChangedHandler(string value);
        public delegate void OnEventHandler();

        public event OnStateHandler OnConsumedStateChanged;
        private bool _consumedValue = false;
        private void ConsumedStateChanged()
        {
            _consumedValue = IsConsumed;
            if (OnConsumedStateChanged != null)
                OnConsumedStateChanged.Invoke(_consumedValue);
        }

        public event OnStateHandler OnLoadedStateChanged;
        private void LoadedStateChanged()
        {
            if (OnLoadedStateChanged != null)
                OnLoadedStateChanged.Invoke(IsLoaded);
        }

        public event OnEventHandler OnStartLoad;
        private void StartedLoad()
        {
            if (OnStartLoad != null)
                OnStartLoad.Invoke();
        }

        public event OnEventHandler OnEndLoad;
        private void EndLoad()
        {
            if (OnEndLoad != null)
                OnEndLoad.Invoke();
        }

        public event OnPriceChangedHandler OnPriceChanged;
        private void PriceChanged()
        {
            if (OnPriceChanged != null)
                OnPriceChanged.Invoke(GetPrice());
        }

        public event OnEventHandler OnStartGet;
        private void StartedGet()
        {
            if (OnStartGet != null)
                OnStartGet.Invoke();
        }

        public event OnEventHandler OnSucceeded;
        private void Succeeded()
        {
            if (OnSucceeded != null)
                OnSucceeded.Invoke();
        }

        public event OnEventHandler OnCancel;
        private void Cancel()
        {
            if (OnCancel != null)
                OnCancel.Invoke();
        }

        public event OnEventHandler OnEndGet;
        private void EndGet()
        {
            if (OnEndGet != null)
                OnEndGet.Invoke();
        }

        #endregion

        protected virtual void Start()
        {
            StartCoroutine(InitInNextFrame());
        }

        private void Update()
        {
            if (_consumedValue != IsConsumed)
                ConsumedStateChanged();
        }

        private IEnumerator InitInNextFrame()
        {
            yield return null;
            ConsumedStateChanged();
            LoadedStateChanged();
            if (LoadOnStart)
                Load();
        }

        public void Load() { Load(false); }

        private void Load(bool startGetIfCompleted)
        {
            if (Purchase != null && _state == State.Idle)
            {
                _state = State.Loading;
                StartedLoad();
                _purchase.Load(() =>
               {
                   _state = State.Idle;
                   EndLoad();
                   LoadedStateChanged();
                   PriceChanged();

                   if (startGetIfCompleted)
                       Get();
               });
            }
        }

        public void Get()
        {
            if (_purchase == null || IsConsumed)
                return;

            if (!IsLoaded)
            {
                Debug.Log("ProductItem->Get->NotLoaded->Load");
                if (CallLoadInGet)
                    Load(true);
            }
            else if (_state == State.Idle)
            {
                _state = State.Getting;
                StartedGet();
                Debug.Log("ProductItem->Get->started request");
                _purchase.Get(result =>
               {
                   _state = State.Idle;
                   Debug.Log("ProductItem->Get->Result:" + result);
                   EndGet();
                   if (result)
                   {
                       Succeeded();
                       PriceChanged();
                   }
                   else
                   {
                       Cancel();
                   }

                   ConsumedStateChanged();
               });
            }
        }
    }
}