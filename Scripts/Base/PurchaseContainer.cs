﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolbox.Market
{
    public abstract class PurchaseContainer : IPurchase
    {
        private IPurchase _purchase;

        public PurchaseContainer(IPurchase purchase)
        {
            _purchase = purchase;
            if (_purchase == null)
                Debug.LogException(new ArgumentNullException("purchase"));
        }

        public virtual string GetKey() { return _purchase.GetKey(); }

        public virtual string GetPrice() { return _purchase.GetPrice(); }

        public virtual bool IsConsumed() { return _purchase.IsConsumed(); }

        public virtual bool IsLoaded() { return _purchase.IsLoaded(); }

        public virtual void Get(Action<bool> cb) { _purchase.Get(cb); }

        public virtual void Load(Action cb) { _purchase.Load(cb); }

        public virtual bool Restore(Dictionary<string, List<string>> data) { return _purchase.Restore(data); }
    }
}
