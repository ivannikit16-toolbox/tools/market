﻿using System;
using System.Collections;
using UnityEngine;

namespace Toolbox.Market
{
    public class RandomWaitingContainer : PurchaseContainer
    {
        private readonly bool _loadWaiting;
        private readonly bool _getWaiting;

        private bool _process = false;

        public override string GetPrice() { return _loaded ? base.GetPrice() : string.Empty; }

        public RandomWaitingContainer(IPurchase purchase, bool loadWaiting = true, bool getWaiting = true) : base(purchase)
        {
            _loadWaiting = loadWaiting;
            _getWaiting = getWaiting;
        }

        private bool _loaded = false;
        public override bool IsLoaded() { return (!_loadWaiting || _loaded) && base.IsLoaded(); }

        public override void Load(Action cb)
        {
            if (!IsLoaded())
            {
                _process = true;
                if (_loadWaiting)
                {
                    Game.Instance.StartCoroutine(
                        RandomDelay(() =>
                       {
                           _loaded = true;
                           base.Load(() =>
                           {
                               _process = false;
                               cb.TryInvoke();
                           });
                       }));
                }
                else
                {
                    _loaded = true;
                    base.Load(() =>
                   {
                       _process = false;
                       cb.TryInvoke(LogMode.Error);
                   });
                }
            }
            else
                cb.TryInvoke(LogMode.Error);
        }

        public override void Get(Action<bool> cb)
        {
            if (!_process && IsLoaded())
            {
                _process = true;
                Action<bool> callback = result =>
                {
                    _process = false;
                    cb.TryInvoke(result, LogMode.Error);
                };

                if (!_getWaiting)
                {
                    base.Get(callback);
                }
                else
                {
                    Game.Instance.StartCoroutine(RandomDelay(() => base.Get(callback)));
                }
            }
        }

        protected IEnumerator RandomDelay(Action callback)
        {
            if (callback != null)
            {
                yield return new WaitForSecondsRealtime(2f + (float)Help.Random.NextDouble() * 2f);
                callback.Invoke();
            }
        }
    }
}
