﻿using System;

namespace Toolbox.Market
{
    public class FreePurchase : Purchase
    {
        private readonly string _price;

        public override string GetPrice()
        {
            string price = string.Empty;
            if (IsLoaded())
            {
                price = _price;
                price = Product.ModifyPrice(price);
            }

            return price;
        }

        public FreePurchase(string key, IProduct product, string price = "Free") : base(key, product)
        {
            _price = price;
        }

        public override bool IsLoaded() { return true; }

        public override void Load(Action cb)
        {
            cb.TryInvoke();
        }

        public override void Get(Action<bool> cb)
        {
            Product.Consume();
            cb.TryInvoke(true);
        }
    }
}
