﻿using System;

namespace Toolbox.Market
{
    public abstract class GCurrencyPurchase : Purchase
    {
        protected int Cost { get; private set; }

        public abstract int Value { get; set; }

        public GCurrencyPurchase(string key, int cost, IProduct product) : base(key, product)
        {
            Cost = cost;
        }

        public override string GetPrice() { return Cost.ToString(); }

        public override bool IsLoaded() { return true; }

        public override void Load(Action cb) { cb.TryInvoke(); }

        public override void Get(Action<bool> cb)
        {
            bool result = false;
            if (Value >= Cost)
            {
                Value -= Cost;
                Product.Consume();
                result = true;
            }

            cb.TryInvoke(result);
        }
    }
}
