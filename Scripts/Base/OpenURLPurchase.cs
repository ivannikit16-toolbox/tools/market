﻿using System;
using UnityEngine;
using System.Collections;

namespace Toolbox.Market
{
    public class OpenURLPurchase : Purchase
    {
        private string _url;
        private bool _requestInProgress = false;

        public OpenURLPurchase(string key, IProduct product, string url) : base(key, product) { _url = url; }

        public override bool IsLoaded() { return !string.IsNullOrEmpty(_url); }

        public override string GetPrice() { return string.Empty; }

        public override void Load(Action cb) { cb.Invoke(); }

        public override void Get(Action<bool> cb)
        {
            if (!IsLoaded() || _requestInProgress)
            {
                UnityEngine.Debug.LogFormat("LikeFacebookPurchase->Get breake IsLoaded: {0}", IsLoaded());
                if (cb != null)
                    cb.Invoke(false);
                return;
            }

            Game.Instance.StartCoroutine(OpenURL(cb));
        }

        private IEnumerator OpenURL(Action<bool> cb)
        {
            bool bgmMuted = SoundManager.Instance.MuteMusic;
            bool sfxMuted = SoundManager.Instance.MuteSfx;
            if (!bgmMuted)
                SoundManager.Instance.MuteMusic = true;
            if (!sfxMuted)
                SoundManager.Instance.MuteSfx = true;
            _requestInProgress = true;

            UnityEngine.Application.OpenURL(_url);
            yield return new WaitForSecondsRealtime(1f);

            SoundManager.Instance.MuteMusic = bgmMuted;
            SoundManager.Instance.MuteSfx = sfxMuted;
            _requestInProgress = false;

            Product.Consume();
            if (cb != null)
                cb.Invoke(true);
        }
    }
}
