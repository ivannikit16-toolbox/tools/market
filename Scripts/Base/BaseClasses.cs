﻿using System;
using System.Collections.Generic;

namespace Toolbox.Market
{
    public interface IProduct
    {
        bool IsConsumed();
        void Consume(bool restore = false);

        [Obsolete()]
        string ModifyPrice(string price);
    }

    public interface IPurchase
    {
        string GetKey();
        bool IsConsumed();
        bool IsLoaded();
        string GetPrice();

        void Load(Action cb);
        void Get(Action<bool> cb);
        bool Restore(Dictionary<string, List<string>> data);
    }

    public abstract class Purchase : IPurchase
    {
        public string Key { get; private set; }
        public string GetKey() { return Key; }

        public IProduct Product { get; private set; }

        public Purchase(string key, IProduct product)
        {
            Key = key;
            Product = product;
        }

        public bool IsConsumed()
        {
            if (Product != null)
                return Product.IsConsumed();
            else
                return false;
        }

        public abstract bool IsLoaded();

        public abstract string GetPrice();

        public abstract void Load(Action cb);
        public abstract void Get(Action<bool> cb);

        public virtual bool Restore(Dictionary<string, List<string>> data) { return false; }
    }
}

