﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolbox.Market
{
    public class PurchaseCollection
    {
        private static PurchaseCollection _instance;
        public static PurchaseCollection Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new PurchaseCollection();
                return _instance;
            }
        }

        private List<IPurchase> _purchases = new List<IPurchase>();

        public void Add(Purchase purchase)
        {
            if (purchase != null)
                _purchases.Add(purchase);
        }

        public void AddRange(params IPurchase[] purchases)
        {
            for (int i = 0; i < purchases.Length; i++)
                _purchases.Add(purchases[i]);
        }

        public void AddRange(IEnumerable<IPurchase> purchases)
        {
            foreach (var p in purchases)
                _purchases.Add(p);
        }

        public IPurchase Find(string key)
        {
            for (int i = 0; i < _purchases.Count; i++)
            {
                if (_purchases[i].GetKey() == key)
                    return _purchases[i];
            }

            Debug.LogErrorFormat("PurchaseCollection: Key \"{0}\" not found", key);
            return null;
        }

        #region Restore

        private List<IRestoreService> _restoreServices = new List<IRestoreService>();
        public void AddRestoreService(IRestoreService svc) { _restoreServices.Add(svc); }

        public void Restore(Action<bool> cb)
        {
            Dictionary<string, List<string>> data = new Dictionary<string, List<string>>(_restoreServices.Count);
            GetRestoreDataRecursive(0, data, () =>
           {
               bool result = false;

               for (int i = 0; i < _purchases.Count; i++)
                   if (_purchases[i].Restore(data))
                       result = true;

               cb.TryInvoke(result);
           });
        }

        private void GetRestoreDataRecursive(int index, Dictionary<string, List<string>> data, Action cb)
        {
            if (cb == null)
                return;

            _restoreServices[index].GetRestorePurchaseData((tag, identifiers) =>
           {
               if (!string.IsNullOrEmpty(tag) && identifiers != null)
                   data.Add(tag, new List<string>(identifiers));
               ++index;
               if (index < _restoreServices.Count)
                   GetRestoreDataRecursive(index, data, cb);
               else
                   cb.Invoke();
           });
        }

        #endregion
    }

    public interface IRestoreService
    {
        void GetRestorePurchaseData(Action<string, string[]> cb);
        string GetRestoreServiceTag();
    }
}
